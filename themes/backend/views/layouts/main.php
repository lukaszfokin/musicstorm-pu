<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Menu', 'url'=>'#', 'items'=>array(
					array('label'=>'Home', 'url'=>array("/adm/site/index")),
					array('label'=>'Administrators', 'url'=>array("/adm/administrator/admin")),
                	array('label'=>'Users', 'url'=>array("/adm/user/admin")),
                	array('label'=>'Genre', 'url'=>array("/adm/genre/admin")),
                	array('label'=>'Artist', 'url'=>array("/adm/artist/admin")),
                	array('label'=>'Album', 'url'=>array("/adm/album/admin")),
				)),
            ),
        ),
		array(
			'class'=>'bootstrap.widgets.TbMenu',
			'htmlOptions'=>array('class'=>'pull-right'),
			'items'=>array(
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/adm/site/logout'), 'visible'=>!Yii::app()->user->isGuest)	
			),
		),
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

</div><!-- page -->

<div id="footer">
	Copyright &copy; <?php echo date('Y'); ?> by Music Storm.<br/>
	All Rights Reserved.<br/>		
</div><!-- footer -->

</body>
</html>
