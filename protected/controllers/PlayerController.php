<?php

class PlayerController extends Controller
{
	public function actionIndex()
	{
		
		$this->layout = "player/main";
		
		if(!empty($_GET['music'])){
			$musicId = base64_decode($_GET['music']);
		}
		
		$music = Track::model()->findByPk($musicId);
		
		$array = array(
				"title"=>$music->TRACK_NAME, 
				"artist"=>$music->getAlbum(),
				"mp3"=>"musics/".$music->TRACK_FILE,
				"poster"=>"images/albums/".$music->ALBUM->ALBUM_IMAGE
		);
		
		$this->render('index', array("music"=>$music, "jsonMusic"=>json_encode($array)));
	}

	public function actionGetPlayerLink(){
		
		if(!empty($_POST["music"])){
			
			$link = $this->createUrl("player/", array("music"=>$_POST["music"]));
			
			echo $link;
			
		}
		
	}
	
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
