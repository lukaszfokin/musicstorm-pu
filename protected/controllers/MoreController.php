<?php

class MoreController extends Controller
{
	public function actionIndex()
	{
		$track = new Track();
		
		if(!empty($_GET['genre'])){
			$genreId = base64_decode($_GET['genre']);
		}
		
		$genre = Genre::model()->findByPk($genreId);
		
		if(empty($genre)){
			echo "MEU OVO!"; exit;
		}
		
		$this->render('index', array("genre"=>$genreId, "track"=>$track));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}