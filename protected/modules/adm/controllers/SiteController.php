<?php

class SiteController extends Controller
{
	
	public function actionIndex()
	{
		
		if(Yii::app()->user->isGuest){			
			$this->redirect($this->createUrl("/adm/site/login"));
		}else{
			$this->render('index');
		}
		
		
		
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		
		
		$this->layout = "login/main";
		
		$model=new LoginForm;
	
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$this->redirect(Yii::app()->getModule('adm')->user->returnUrl);
			}
		}
		
		
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect($this->createUrl("/adm/site/login"));
	}	
}