<?php

/**
 * This is the model class for table "USER".
 *
 * The followings are the available columns in table 'USER':
 * @property integer $ID_USER
 * @property string $USER_NAME
 * @property string $USER_LAST_NAME
 * @property string $LOGIN
 * @property string $PASSWORD
 * @property string $EMAIL
 * @property string $REG_DATE
 * @property integer $FREE_DAYS
 * @property integer $STATUS
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'USER';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('USER_NAME, USER_LAST_NAME, LOGIN, PASSWORD, EMAIL, REG_DATE, FREE_DAYS, STATUS', 'required'),
			array('FREE_DAYS, STATUS', 'numerical', 'integerOnly'=>true),
			array('USER_NAME, USER_LAST_NAME', 'length', 'max'=>100),
			array('LOGIN, PASSWORD', 'length', 'max'=>45),
			array('EMAIL', 'length', 'max'=>60),
			array('EMAIL', 'unique'),
			array('EMAIL', 'email'),
			array('LOGIN', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_USER, USER_NAME, USER_LAST_NAME, LOGIN, PASSWORD, EMAIL, REG_DATE, FREE_DAYS, STATUS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_USER' => 'Id User',
			'USER_NAME' => 'Name',
			'USER_LAST_NAME' => 'Last Name',
			'LOGIN' => 'Login',
			'PASSWORD' => 'Password',
			'EMAIL' => 'Email',
			'REG_DATE' => 'Reg Date',
			'FREE_DAYS' => 'Free Days',
			'STATUS' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_USER',$this->ID_USER);
		$criteria->compare('USER_NAME',$this->USER_NAME,true,'OR');
		$criteria->compare('USER_LAST_NAME',$this->USER_NAME,true,'OR');
		$criteria->compare('LOGIN',$this->USER_NAME,true,'OR');
		$criteria->compare('PASSWORD',$this->PASSWORD,true);
		$criteria->compare('EMAIL',$this->USER_NAME,true,'OR');
		$criteria->compare('REG_DATE',$this->REG_DATE,true);
		$criteria->compare('FREE_DAYS',$this->FREE_DAYS);
		$criteria->compare('STATUS',$this->STATUS);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeValidate(){
		$this->REG_DATE = date("Y-m-d H:i:s");
		
		return parent::beforeValidate();
	}
}