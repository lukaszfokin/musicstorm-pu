<?php

/**
 * This is the model class for table "ARTIST".
 *
 * The followings are the available columns in table 'ARTIST':
 * @property integer $ID_ARTIST
 * @property string $ARTIST_NAME
 *
 * The followings are the available model relations:
 * @property ALBUMARTIST[] $aLBUMARTISTs
 * @property ARTISTGENRE[] $aRTISTGENREs
 */
class Artist extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Artist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ARTIST';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ARTIST_NAME, GENRES', 'required'),
			array('ARTIST_NAME', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_ARTIST, ARTIST_NAME', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ALBUMS' => array(self::HAS_MANY, 'ALBUMARTIST', 'ID_ARTIST'),
			'GENRES' => array(self::HAS_MANY, 'ArtistGenre', 'ID_ARTIST'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_ARTIST' => 'Id Artist',
			'ARTIST_NAME' => 'Artist Name',
			'genresForm'=> 'Genres',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_ARTIST',$this->ID_ARTIST);
		$criteria->compare('ARTIST_NAME',$this->ARTIST_NAME,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function afterSave(){
		ArtistGenre::model()->deleteAll("ID_ARTIST = :ID", array(":ID"=>$this->ID_ARTIST));
		
		foreach($this->GENRES as $genre){		
			$artistGenre = new ArtistGenre();
			$artistGenre->ID_ARTIST = $this->ID_ARTIST;
			$artistGenre->ID_GENRE  = $genre;
			if(!$artistGenre->save()){
				var_dump($artistGenre->getErrors()); exit;
			}
		}
		
		return parent::beforeSave();	
	}
	
	public function getArtists(){
		$artists = $this->findAll(array('order'=>'ARTIST_NAME'));
		
		if(!empty($artists)){
			return CHtml::listData($artists, 'ID_ARTIST', 'ARTIST_NAME');
		}
	}
}