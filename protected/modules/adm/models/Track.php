<?php

/**
 * This is the model class for table "TRACK".
 *
 * The followings are the available columns in table 'TRACK':
 * @property integer $ID_TRACK
 * @property string $TRACK_NAME
 * @property integer $TRACK_ALBUM
 * @property string $TRACK_FILE
 *
 * The followings are the available model relations:
 * @property ALBUM $tRACKALBUM
 */
class Track extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Track the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'TRACK';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TRACK_NAME, TRACK_ALBUM, TRACK_FILE', 'required'),
			array('TRACK_ALBUM', 'numerical', 'integerOnly'=>true),
			array('TRACK_NAME', 'length', 'max'=>250),
			array('TRACK_FILE', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_TRACK, TRACK_NAME, TRACK_ALBUM, TRACK_FILE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tRACKALBUM' => array(self::BELONGS_TO, 'ALBUM', 'TRACK_ALBUM'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_TRACK' => 'Id Track',
			'TRACK_NAME' => 'Track Name',
			'TRACK_ALBUM' => 'Track Album',
			'TRACK_FILE' => 'Track File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_TRACK',$this->ID_TRACK);
		$criteria->compare('TRACK_NAME',$this->TRACK_NAME,true);
		$criteria->compare('TRACK_ALBUM',$this->TRACK_ALBUM);
		$criteria->compare('TRACK_FILE',$this->TRACK_FILE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}