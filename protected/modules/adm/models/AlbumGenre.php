<?php

/**
 * This is the model class for table "ALBUM_GENRE".
 *
 * The followings are the available columns in table 'ALBUM_GENRE':
 * @property integer $ID_ALBUM_GENRE
 * @property integer $ID_ALBUM
 * @property integer $ID_GENRE
 *
 * The followings are the available model relations:
 * @property ALBUM $iDALBUM
 * @property GENRE $iDGENRE
 */
class AlbumGenre extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AlbumGenre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ALBUM_GENRE';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_ALBUM, ID_GENRE', 'required'),
			array('ID_ALBUM, ID_GENRE', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_ALBUM_GENRE, ID_ALBUM, ID_GENRE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDALBUM' => array(self::BELONGS_TO, 'ALBUM', 'ID_ALBUM'),
			'iDGENRE' => array(self::BELONGS_TO, 'GENRE', 'ID_GENRE'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_ALBUM_GENRE' => 'Id Album Genre',
			'ID_ALBUM' => 'Id Album',
			'ID_GENRE' => 'Id Genre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_ALBUM_GENRE',$this->ID_ALBUM_GENRE);
		$criteria->compare('ID_ALBUM',$this->ID_ALBUM);
		$criteria->compare('ID_GENRE',$this->ID_GENRE);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}