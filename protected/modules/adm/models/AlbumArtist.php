<?php

/**
 * This is the model class for table "ALBUM_ARTIST".
 *
 * The followings are the available columns in table 'ALBUM_ARTIST':
 * @property integer $ID_ALBUM_ARTIST
 * @property integer $ID_ALBUM
 * @property integer $ID_ARTIST
 *
 * The followings are the available model relations:
 * @property ALBUM $iDALBUM
 * @property ARTIST $iDARTIST
 */
class AlbumArtist extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AlbumArtist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ALBUM_ARTIST';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_ALBUM, ID_ARTIST', 'required'),
			array('ID_ALBUM, ID_ARTIST', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_ALBUM_ARTIST, ID_ALBUM, ID_ARTIST', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDALBUM' => array(self::BELONGS_TO, 'ALBUM', 'ID_ALBUM'),
			'iDARTIST' => array(self::BELONGS_TO, 'ARTIST', 'ID_ARTIST'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_ALBUM_ARTIST' => 'Id Album Artist',
			'ID_ALBUM' => 'Id Album',
			'ID_ARTIST' => 'Id Artist',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_ALBUM_ARTIST',$this->ID_ALBUM_ARTIST);
		$criteria->compare('ID_ALBUM',$this->ID_ALBUM);
		$criteria->compare('ID_ARTIST',$this->ID_ARTIST);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}