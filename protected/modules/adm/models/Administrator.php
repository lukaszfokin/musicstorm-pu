<?php

/**
 * This is the model class for table "ADM".
 *
 * The followings are the available columns in table 'ADM':
 * @property integer $ID_ADM
 * @property string $ADM_NAME
 * @property string $ADM_LAST_NAME
 * @property string $LOGIN
 * @property string $PASSWORD
 * @property string $EMAIL
 * @property integer $STATUS
 */
class Administrator extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Administrator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ADM';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ADM_NAME, ADM_LAST_NAME, LOGIN, PASSWORD, EMAIL, STATUS', 'required'),
			array('STATUS', 'numerical', 'integerOnly'=>true),
			array('ADM_NAME, ADM_LAST_NAME', 'length', 'max'=>100),
			array('LOGIN, PASSWORD', 'length', 'max'=>45),
			array('EMAIL', 'length', 'max'=>60),
			array('EMAIL', 'email'),
			array('EMAIL', 'unique'),
			array('LOGIN', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_ADM, ADM_NAME, ADM_LAST_NAME, LOGIN, PASSWORD, EMAIL, STATUS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_ADM' => 'Id',
			'ADM_NAME' => 'Name',
			'ADM_LAST_NAME' => 'Last Name',
			'LOGIN' => 'Login',
			'PASSWORD' => 'Password',
			'EMAIL' => 'Email',
			'STATUS' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_ADM',$this->ID_ADM);
		$criteria->compare('ADM_NAME',$this->ADM_NAME,true, "OR");
		$criteria->compare('ADM_LAST_NAME',$this->ADM_NAME,true);
		$criteria->compare('LOGIN',$this->ADM_NAME,true, "OR");
		$criteria->compare('PASSWORD',$this->PASSWORD,true);
		$criteria->compare('EMAIL',$this->ADM_NAME,true, "OR");
		$criteria->compare('STATUS',$this->STATUS);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}