<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Users')=>array('admin'),
	$model->ID_USER=>array('view','id'=>$model->ID_USER),
	Yii::t(Yii::app()->language,'Update'),
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'View'), 'url'=>array('view', 'id'=>$model->ID_USER)),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Update'); ?> User <?php echo $model->ID_USER; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>