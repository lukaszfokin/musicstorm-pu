<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Users')=>array('admin'),
	$model->ID_USER,
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'Update'), 'url'=>array('update', 'id'=>$model->ID_USER)),
	array('label'=>Yii::t(Yii::app()->language,'Delete'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_USER),'confirm'=>Yii::t(Yii::app()->language,'Are you sure you want to delete this item?'))),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'View'); ?> User #<?php echo $model->ID_USER; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'type'=>'striped bordered condensed',
	'data'=>$model,
	'attributes'=>array(
		'ID_USER',
		'USER_NAME',
		'USER_LAST_NAME',
		'LOGIN',
		'PASSWORD',
		'EMAIL',
		'REG_DATE',
		'FREE_DAYS',
		array('name'=>'STATUS', 'value'=>Helpers::getStatus($model->STATUS), 'type'=>'raw'),
	),
)); ?>
