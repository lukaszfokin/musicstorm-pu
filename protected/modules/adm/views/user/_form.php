<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t(Yii::app()->language,'Fields with'); ?><span class="required"> * </span><?php echo Yii::t(Yii::app()->language,'are required'); ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'USER_NAME',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'USER_LAST_NAME',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'LOGIN',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->passwordFieldRow($model,'PASSWORD',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'EMAIL',array('class'=>'span5','maxlength'=>60)); ?>

	<?php echo $form->textFieldRow($model,'FREE_DAYS',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'STATUS',array('1'=>'Active', '0'=>'Inactive')); ?>

	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t(Yii::app()->language,'Create') : Yii::t(Yii::app()->language,'Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
