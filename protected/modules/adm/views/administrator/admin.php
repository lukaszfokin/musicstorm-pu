<?php
$this->breadcrumbs=array(
 	Yii::t(Yii::app()->language,'Administrators')=>array('admin'),
	Yii::t(Yii::app()->language,'Manage'),
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('administrator-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t(Yii::app()->language,'Manage'); ?> Administrators</h1>

<div class="search-form">
<?php  

$this->widget('bootstrap.widgets.TbMenu', array(
		'type'=>'pills', // '', 'tabs', 'pills' (or 'list')
		'stacked'=>false, // whether this is a stacked menu
		'items'=>array(
				array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create'),'active'=>true),
		),
));
				
$this->renderPartial('_search',array(
	'model'=>$model,
));  ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'administrator-grid',
	'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'ID_ADM',
		'ADM_NAME',
		'LOGIN',
		'EMAIL',
		array('name'=>'STATUS', 'value'=>'Helpers::getStatus($data->STATUS)', 'type'=>'raw'),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
