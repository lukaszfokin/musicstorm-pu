<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'searchForm',
    'type'=>'search'
)); ?>

<?php echo $form->textFieldRow($model, 'ADM_NAME', array('class'=>'input-xlarge', 'prepend'=>'<i class="icon-search"></i>')); ?>
	
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'label'=>Yii::t(Yii::app()->language,'Search'),
		)); ?>


<?php $this->endWidget(); ?>
