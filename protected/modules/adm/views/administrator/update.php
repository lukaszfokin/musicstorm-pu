<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Administrators')=>array('admin'),
	$model->ID_ADM=>array('view','id'=>$model->ID_ADM),
	Yii::t(Yii::app()->language,'Update'),
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'View'), 'url'=>array('view', 'id'=>$model->ID_ADM)),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Update'); ?> Administrator <?php echo $model->ID_ADM; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>