<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Administrators'),
);

$this->menu=array(
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Administrators'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
