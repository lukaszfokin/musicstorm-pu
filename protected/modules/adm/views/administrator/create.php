<?php
$this->breadcrumbs=array(
 	Yii::t(Yii::app()->language,'Administrators')=>array('admin'),
	Yii::t(Yii::app()->language,'Create'),
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Create'); ?> Administrator</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>