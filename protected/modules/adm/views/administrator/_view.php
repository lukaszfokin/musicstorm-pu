<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_ADM')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_ADM),array('view','id'=>$data->ID_ADM)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ADM_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->ADM_NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ADM_LAST_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->ADM_LAST_NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LOGIN')); ?>:</b>
	<?php echo CHtml::encode($data->LOGIN); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PASSWORD')); ?>:</b>
	<?php echo CHtml::encode($data->PASSWORD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EMAIL')); ?>:</b>
	<?php echo CHtml::encode($data->EMAIL); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('STATUS')); ?>:</b>
	<?php echo CHtml::encode($data->STATUS); ?>
	<br />


</div>