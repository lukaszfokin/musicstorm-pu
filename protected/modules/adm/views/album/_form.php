<?php Yii::app()->clientScript->registerScript("deleteMusic", "
	$('.deleteMusic').click(function(){
		
	});
");
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'album-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	)
)); ?>

	<p class="note"><?php echo Yii::t(Yii::app()->language,'Fields with'); ?><span class="required"> * </span><?php echo Yii::t(Yii::app()->language,'are required'); ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'ALBUM_NAME',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'ALBUM_Y_RELEASE',array('class'=>'span5')); ?>

	<?php echo $form->fileFieldRow($model,'ALBUM_IMAGE'); ?>
	
	<?php echo $model->getImage();?>	

	<?php echo $form->textFieldRow($model,'QTY_TRACKS',array('class'=>'span1')); ?>
	
	<?php echo $form->dropDownListRow($model, 'ARTISTS', Artist::model()->getArtists(), array('class'=>'span5', 'multiple'=>'multiple'))?>
	
	<?php echo $form->dropDownListRow($model, 'GENRES', Genre::model()->getGenres(), array('class'=>'span5', 'multiple'=>'multiple'))?>
	
	<?php echo $form->fileFieldRow($model,'TRACKS[]', array('multiple'=>'multiple')); ?>
	
	<?php echo $model->getMusics();?>

	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t(Yii::app()->language,'Create') : Yii::t(Yii::app()->language,'Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
