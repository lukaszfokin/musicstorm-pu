<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_ALBUM')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_ALBUM),array('view','id'=>$data->ID_ALBUM)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ALBUM_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->ALBUM_NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ALBUM_Y_RELEASE')); ?>:</b>
	<?php echo CHtml::encode($data->ALBUM_Y_RELEASE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ALBUM_IMAGE')); ?>:</b>
	<?php echo CHtml::encode($data->ALBUM_IMAGE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('QTY_TRACKS')); ?>:</b>
	<?php echo CHtml::encode($data->QTY_TRACKS); ?>
	<br />


</div>