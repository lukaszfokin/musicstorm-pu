<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Genres')=>array('admin'),
	$model->ID_GENRE,
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'Update'), 'url'=>array('update', 'id'=>$model->ID_GENRE)),
	array('label'=>Yii::t(Yii::app()->language,'Delete'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_GENRE),'confirm'=>Yii::t(Yii::app()->language,'Are you sure you want to delete this item?'))),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'View'); ?> Genre #<?php echo $model->ID_GENRE; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'type'=>'striped bordered condensed',
	'data'=>$model,
	'attributes'=>array(
		'ID_GENRE',
		'GENRE_NAME',
	),
)); ?>
