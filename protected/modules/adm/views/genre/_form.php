<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'genre-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t(Yii::app()->language,'Fields with'); ?><span class="required"> * </span><?php echo Yii::t(Yii::app()->language,'are required'); ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'GENRE_NAME',array('class'=>'span5','maxlength'=>45)); ?>

	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t(Yii::app()->language,'Create') : Yii::t(Yii::app()->language,'Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
