<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Genres'),
);

$this->menu=array(
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Genres'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
