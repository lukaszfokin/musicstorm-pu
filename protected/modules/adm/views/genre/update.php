<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Genres')=>array('admin'),
	$model->ID_GENRE=>array('view','id'=>$model->ID_GENRE),
	Yii::t(Yii::app()->language,'Update'),
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'View'), 'url'=>array('view', 'id'=>$model->ID_GENRE)),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Update'); ?> Genre <?php echo $model->ID_GENRE; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>