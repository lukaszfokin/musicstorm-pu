<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_GENRE')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_GENRE),array('view','id'=>$data->ID_GENRE)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GENRE_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->GENRE_NAME); ?>
	<br />


</div>