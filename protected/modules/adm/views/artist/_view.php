<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_ARTIST')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_ARTIST),array('view','id'=>$data->ID_ARTIST)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ARTIST_NAME')); ?>:</b>
	<?php echo CHtml::encode($data->ARTIST_NAME); ?>
	<br />


</div>