<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Artists')=>array('admin'),
	$model->ID_ARTIST=>array('view','id'=>$model->ID_ARTIST),
	Yii::t(Yii::app()->language,'Update'),
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'View'), 'url'=>array('view', 'id'=>$model->ID_ARTIST)),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'Update'); ?> Artist <?php echo $model->ID_ARTIST; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>