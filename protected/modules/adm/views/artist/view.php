<?php
$this->breadcrumbs=array(
	Yii::t(Yii::app()->language,'Artists')=>array('admin'),
	$model->ID_ARTIST,
);

$this->menu=array(
	//array('label'=>Yii::t(Yii::app()->language,'List'), 'url'=>array('index')),
	array('label'=>Yii::t(Yii::app()->language,'Create'), 'url'=>array('create')),
	array('label'=>Yii::t(Yii::app()->language,'Update'), 'url'=>array('update', 'id'=>$model->ID_ARTIST)),
	array('label'=>Yii::t(Yii::app()->language,'Delete'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_ARTIST),'confirm'=>Yii::t(Yii::app()->language,'Are you sure you want to delete this item?'))),
	array('label'=>Yii::t(Yii::app()->language,'Manage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t(Yii::app()->language,'View'); ?> Artist #<?php echo $model->ID_ARTIST; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'type'=>'striped bordered condensed',
	'data'=>$model,
	'attributes'=>array(
		'ID_ARTIST',
		'ARTIST_NAME',
	),
)); ?>
