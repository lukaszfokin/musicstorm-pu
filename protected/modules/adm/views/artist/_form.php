<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'artist-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t(Yii::app()->language,'Fields with'); ?><span class="required"> * </span><?php echo Yii::t(Yii::app()->language,'are required'); ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'ARTIST_NAME',array('class'=>'span5','maxlength'=>45)); ?>
	
	<?php echo $form->dropDownListRow($model, 'GENRES', Genre::model()->getGenres(), array('multiple'=>'multiple', 'class'=>'span5'))?>

	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t(Yii::app()->language,'Create') : Yii::t(Yii::app()->language,'Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>
