<?php

/**
 * This is the model class for table "TRACK".
 *
 * The followings are the available columns in table 'TRACK':
 * @property integer $ID_TRACK
 * @property string $TRACK_NAME
 * @property integer $TRACK_ALBUM
 * @property string $TRACK_FILE
 *
 * The followings are the available model relations:
 * @property ALBUM $tRACKALBUM
 */
class Track extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Track the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'TRACK';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TRACK_NAME, TRACK_ALBUM, TRACK_FILE', 'required'),
			array('TRACK_ALBUM', 'numerical', 'integerOnly'=>true),
			array('TRACK_NAME', 'length', 'max'=>45),
			array('TRACK_FILE', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_TRACK, TRACK_NAME, TRACK_ALBUM, TRACK_FILE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ALBUM' => array(self::BELONGS_TO, 'Album', 'TRACK_ALBUM'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_TRACK' => 'Id Track',
			'TRACK_NAME' => 'Track Name',
			'TRACK_ALBUM' => 'Track Album',
			'TRACK_FILE' => 'Track File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_TRACK',$this->ID_TRACK);
		$criteria->compare('TRACK_NAME',$this->TRACK_NAME,true);
		$criteria->compare('TRACK_ALBUM',$this->TRACK_ALBUM);
		$criteria->compare('TRACK_FILE',$this->TRACK_FILE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getTracks($idGenre = "", $termoBusca = ""){
		
		if(empty($idGenre)){		
			$sql = "SELECT ID_GENRE, ID_ALBUM FROM ALBUM_GENRE GROUP BY ID_GENRE";		
			$relGenreAlbum = Yii::app()->db->createCommand($sql)->queryAll();
			$limitTrack = "LIMIT 16";
			$hideMore = false;
		}else{
			$sql = "SELECT ID_GENRE, ID_ALBUM FROM ALBUM_GENRE WHERE ID_GENRE = $idGenre GROUP BY ID_GENRE";
			$relGenreAlbum = Yii::app()->db->createCommand($sql)->queryAll();
			$limitTrack = "";
			$hideMore = true;
		}
		
		if(!empty($termoBusca)){
			$like = "AND t.TRACK_NAME LIKE '%$termoBusca%'";
		}else{
			$like = "";
		}
		
		foreach($relGenreAlbum as $genreAlbum){		
			
			$sql = "SELECT GENRE_NAME, ID_GENRE FROM GENRE WHERE ID_GENRE = :ID";
			
			
			$genres = Yii::app()->db->createCommand($sql)->queryAll(true, array(":ID"=>$genreAlbum["ID_GENRE"]));
			
			foreach($genres as $genre){
				
				echo CController::renderInternal(Yii::app()->basePath."/views/site/_genreTitle.php", array("title"=>$genre["GENRE_NAME"], "idGenre"=>$genre["ID_GENRE"], "hideMore"=>$hideMore));
			
				
				$sql= "SELECT A.* FROM ALBUM A
					   JOIN ALBUM_GENRE AG ON (AG.ID_ALBUM = A.ID_ALBUM)
					   WHERE AG.ID_GENRE = :GENRE";
				
				$albums = Yii::app()->db->createCommand($sql)->queryAll(true, array(":GENRE"=>$genre["ID_GENRE"]));
				
				$idsAlbum = array();
				
				foreach($albums as $album){
					$idsAlbum[] = $album["ID_ALBUM"];
				}
				
				if(!empty($idsAlbum)){
					$idsAlbum = implode(",", $idsAlbum);
					
				}
				
				$sql = "SELECT t.*, a.ALBUM_IMAGE, a.ALBUM_NAME FROM TRACK t 
						JOIN ALBUM a ON (a.ID_ALBUM = t. TRACK_ALBUM)
						WHERE t.TRACK_ALBUM IN($idsAlbum) $like ORDER BY RAND() $limitTrack";

				$tracks = Yii::app()->db->createCommand($sql)->queryAll();				
				
				echo "<ul class='border'>";
				
				foreach($tracks as $track){
					
					$track["TRACK_NAME"] = $this->abreviaString(str_replace(".mp3", "", $track["TRACK_NAME"]), 12);
					
					
					
					echo CController::renderInternal(Yii::app()->basePath."/views/site/_genreMusics.php", array("image"=>$track["ALBUM_IMAGE"], "trackName"=>$track["TRACK_NAME"], "albumName"=>$track["ALBUM_NAME"], "trackId"=>$track["ID_TRACK"]));
				}
				
				echo "</ul>";			
				
			}					
		}				
	}
	
	public function getAlbum(){
		return $this->ALBUM->ALBUM_NAME;
	}
	
	public static function somenteTexto($string){
		$trans_tbl = get_html_translation_table(HTML_ENTITIES);
		$trans_tbl = array_flip($trans_tbl);
		return trim(strip_tags(strtr($string, $trans_tbl)));
	}
	
	public function abreviaString($texto, $limite, $tres_p = '...')
	{
		$totalCaracteres = 0;
		//Retorna o texto em plain/text
		$texto = $this->somenteTexto($texto);
		//Cria um array com todas as palavras do texto
		$vetorPalavras = explode(" ",$texto);
		if(strlen($texto) <= $limite):
		$tres_p = "";
		$novoTexto = $texto;
		else:
		//Começa a criar o novo texto resumido.
		$novoTexto = "";
		//Acrescenta palavra por palavra na string enquanto ela
		//não exceder o tamanho máximo do resumo
		for($i = 0; $i <count($vetorPalavras); $i++):
		$totalCaracteres += strlen(" ".$vetorPalavras[$i]);
		if($totalCaracteres <= $limite)
			$novoTexto .= ' ' . $vetorPalavras[$i];
		else break;
		endfor;
		endif;
		return $novoTexto . $tres_p;
	}
}