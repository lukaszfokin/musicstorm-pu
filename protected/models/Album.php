<?php

/**
 * This is the model class for table "ALBUM".
 *
 * The followings are the available columns in table 'ALBUM':
 * @property integer $ID_ALBUM
 * @property string $ALBUM_NAME
 * @property integer $ALBUM_Y_RELEASE
 * @property string $ALBUM_IMAGE
 * @property integer $QTY_TRACKS
 *
 * The followings are the available model relations:
 * @property ALBUMARTIST[] $aLBUMARTISTs
 * @property ALBUMGENRE[] $aLBUMGENREs
 */
class Album extends CActiveRecord
{
	public $imagePath;
	public $imageUrl;
	public $musicPath;
	public $musicUrl;
	public $imageUpload = false;
	public $musicUpload = false;
	
	public function init(){
		$this->imagePath = Yii::app()->basePath."/../images/albums/";
		$this->imageUrl  = Yii::app()->baseUrl."/images/albums/";
		$this->musicPath = Yii::app()->basePath."/../musics/";
		$this->musicUrl  = Yii::app()->baseUrl."/musics/";
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Album the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ALBUM';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ALBUM_NAME, ALBUM_Y_RELEASE, QTY_TRACKS, ARTISTS, GENRES', 'required'),
			array('ALBUM_Y_RELEASE, QTY_TRACKS', 'numerical', 'integerOnly'=>true),
			array('ALBUM_NAME', 'length', 'max'=>45),
			array('ALBUM_IMAGE', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_ALBUM, ALBUM_NAME, ALBUM_Y_RELEASE, ALBUM_IMAGE, QTY_TRACKS', 'safe', 'on'=>'search'),
			array('ALBUM_IMAGE', 'required', 'on'=>'create'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ARTISTS' => array(self::HAS_MANY, 'AlbumArtist', 'ID_ALBUM'),
			'GENRES' => array(self::HAS_MANY, 'AlbumGenre', 'ID_ALBUM'),
			'TRACKS' => array(self::HAS_MANY, 'Track', 'TRACK_ALBUM'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_ALBUM' => 'Id Album',
			'ALBUM_NAME' => 'Album Name',
			'ALBUM_Y_RELEASE' => 'Year Release',
			'ALBUM_IMAGE' => 'Album Image',
			'QTY_TRACKS' => 'Qty Tracks',
			'TRACKS' =>'Tracks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_ALBUM',$this->ID_ALBUM);
		$criteria->compare('ALBUM_NAME',$this->ALBUM_NAME,true);
		$criteria->compare('ALBUM_Y_RELEASE',$this->ALBUM_Y_RELEASE);
		$criteria->compare('ALBUM_IMAGE',$this->ALBUM_IMAGE,true);
		$criteria->compare('QTY_TRACKS',$this->QTY_TRACKS);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function afterSave(){
		
		AlbumGenre::model()->deleteAll("ID_ALBUM = :ID", array(":ID"=>$this->ID_ALBUM));
		AlbumArtist::model()->deleteAll("ID_ALBUM = :ID", array(":ID"=>$this->ID_ALBUM));
		
		foreach($this->GENRES as $genre){
			$model = new AlbumGenre();
			$model->ID_ALBUM = $this->ID_ALBUM;
			$model->ID_GENRE = $genre;			
 			$model->save();
		}
		
		foreach($this->ARTISTS as $artist){
			$model = new AlbumArtist();
			$model->ID_ALBUM = $this->ID_ALBUM;
			$model->ID_ARTIST = $artist;
			$model->save();
		}
		
		if(!empty($this->ALBUM_IMAGE) AND $this->imageUpload){
			$img = CUploadedFile::getInstance($this, 'ALBUM_IMAGE');
			$img->saveAs($this->imagePath.$this->ALBUM_IMAGE); 
		}
		
		
		if(!empty($this->TRACKS) AND $this->musicUpload){
		
			$tracks = CUploadedFile::getInstances($this, 'TRACKS');
				
			foreach($tracks as $track){
				
				$model = new Track();
				$model->TRACK_NAME = ucfirst(str_ireplace("_", " ", $track->name));
				$model->TRACK_ALBUM = $this->ID_ALBUM;
				$model->TRACK_FILE = $track->name;
				$model->save();				
				
				$track->saveAs($this->musicPath.$track->name);
			}
		
		}
			
		
		return parent::beforeSave();
	}
	
	public function getImage(){
		
		if(!empty($this->ALBUM_IMAGE)){			
			$img = "<a href='#' class='thumbnail' style='width:160px; margin:10px 0 10px 0;'><img src='$this->imageUrl$this->ALBUM_IMAGE'></a>";
			return $img;
		}		
	}
	
	public function getAlbums(){
		$model = $this->findAll(array("order"=>"ALBUM_NAME"));
		return CHtml::listData($model, 'ID_ALBUM', 'ALBUM_NAME');
	}
	
	
	public function getMusics(){
		
		if(!empty($this->TRACKS)){
			
			foreach($this->TRACKS as $track){

				
				
			echo "<br>";	
				
			$excluir = "<span><span class='required deleteMusic' id='$track->ID_TRACK'>X</span>";
			
			echo CHtml::label("$track->TRACK_NAME $excluir", "");	
				
			echo "<audio controls class='audioAlbum'>
					<source src=\"$this->musicUrl$track->TRACK_FILE\" type=\"audio/mpeg\" />
				</audio></span>";
			
			}
			
		}
		
	}
}