<?php

/**
 * This is the model class for table "ARTIST_GENRE".
 *
 * The followings are the available columns in table 'ARTIST_GENRE':
 * @property integer $ID_ARTIST_GENRE
 * @property integer $ID_ARTIST
 * @property integer $ID_GENRE
 *
 * The followings are the available model relations:
 * @property GENRE $iDGENRE
 * @property ARTIST $iDARTIST
 */
class ArtistGenre extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ArtistGenre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ARTIST_GENRE';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_ARTIST, ID_GENRE', 'required'),
			array('ID_ARTIST, ID_GENRE', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_ARTIST_GENRE, ID_ARTIST, ID_GENRE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDGENRE' => array(self::BELONGS_TO, 'GENRE', 'ID_GENRE'),
			'iDARTIST' => array(self::BELONGS_TO, 'ARTIST', 'ID_ARTIST'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_ARTIST_GENRE' => 'Id Artist Genre',
			'ID_ARTIST' => 'Id Artist',
			'ID_GENRE' => 'Id Genre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_ARTIST_GENRE',$this->ID_ARTIST_GENRE);
		$criteria->compare('ID_ARTIST',$this->ID_ARTIST);
		$criteria->compare('ID_GENRE',$this->ID_GENRE);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}