<?php

/**
 * This is the model class for table "GENRE".
 *
 * The followings are the available columns in table 'GENRE':
 * @property integer $ID_GENRE
 * @property string $GENRE_NAME
 */
class Genre extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Genre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'GENRE';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('GENRE_NAME', 'required'),
			array('GENRE_NAME', 'length', 'max'=>45),
			array('GENRE_NAME', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID_GENRE, GENRE_NAME', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_GENRE' => 'Id Genre',
			'GENRE_NAME' => 'Genre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_GENRE',$this->ID_GENRE);
		$criteria->compare('GENRE_NAME',$this->GENRE_NAME,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getGenres(){
		$genres = $this->findAll(array("order"=>"GENRE_NAME"));
		return CHtml::listData($genres, 'ID_GENRE', 'GENRE_NAME');
	}
}