<?php

class Helpers{
	
	public function getStatus($status){
		
		if($status == 1){
			$type  = "success";
			$label = "Active";
		}else{
			$type  = "important";
			$label = "Inactive";
		}
		
		return "<span class=\"label label-$type\">$label</span>";
	}
	
	
	public function getDate($date){
		
		if(!empty($date)){
			$date = strtotime($date);
			return date("d/m/Y H:i:s", $date);
		}
		
	}
	
}