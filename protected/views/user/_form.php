<?php
Yii::app()->clientScript->registerCss("content","
	.content{
		border-bottom: none;
	}
");
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<?php if(Yii::app()->user->hasFlash('createUser')): ?>

	<div class="alert alert-block alert-success">
		<?php echo Yii::app()->user->getFlash('createUser'); ?>
	</div>

	<?php else: ?>

	<?php echo $form->textFieldRow($model,'USER_NAME',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'USER_LAST_NAME',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'LOGIN',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->passwordFieldRow($model,'PASSWORD',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'EMAIL',array('class'=>'span5','maxlength'=>60)); ?>

	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? Yii::t(Yii::app()->language,'Create') : Yii::t(Yii::app()->language,'Save'),
		)); ?>
	</div>
	
	<?php endif;?>

<?php $this->endWidget(); ?>
