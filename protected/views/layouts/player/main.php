<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Mobile viewport optimized -->
<meta name="viewport" content="width=device-width, max-scale=-1, user-scalable=no">
<meta name="title" content="<?php echo CHtml::encode($this->pageTitle);?>">
<title><?php echo CHtml::encode($this->pageTitle);?></title>



</head>
<body>
<?php echo $content;?>
