<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/yii.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
	
<?php 
Yii::app()->clientScript->registerScript("playMusic", "

	$('.playMusic').click(function(){
		
		link = '".$this->createUrl('player/')."&music='+$(this).attr('id')+'';

		popup = window.open(link, 'MusicStorm', 'toolbar=yes,menubar=yes,resizable=yes,status=no,scrollbars=yes,width=515,height=485');
				
		return false;	
		
	});			
		
", CClientScript::POS_READY);
?>
</head>


<body>

<div class="container" id="page">
<header>
	<div id="header">
		<div id="headerContent">
			<div id="logo">
				<a href="index.php" title="Music Storm"><h1 id="logotipo">Music Storm</h1></a>
			</div>
			
			<form class="form-search pull-left" id="searchForm" action="<?php echo Yii::app()->createUrl("site/busca");?>" method="post">						 
				<div class="input-prepend">
	    		<input class="input-large" placeholder="Find here" name="busca" id="search" type="text" value="<?php echo @$_POST["busca"]; ?>" /></div>						
	    		<button class="btn" type="submit" name="ir">Search</button>						 
			</form>			
			
			<div id="access">
				  <?php if(Yii::app()->user->isGuest){?>
				  <a class="login" href="<?php echo Yii::app()->createUrl("site/login")?>" title=""> Login </a>
				  <a class="register" href="<?php echo Yii::app()->createUrl("user/create")?>" title="Sign up"> Sign up </a>
				  <?php }else{?>
				  <a class="login" href="<?php echo Yii::app()->createUrl("site/logout")?>" title=""> Logout (<?php echo Yii::app()->user->name?>) </a>
				  <?php } ?>
			</div>
			
		</div>
	</div><!-- header -->
</header>


	<?php echo $content; ?>

	<div class="clear"></div>

<footer>
	<div id="footer">
		<div id="footerContent">
			
			<div id="mainmenu">
				<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>array(
					array('label'=>'Home', 'url'=>array('site/index')),
					array('label'=>'About', 'url'=>array('site/page', 'view'=>'about')),
					array('label'=>'Contact', 'url'=>array('site/contact')),
					),
				)); ?>
			</div>
		
			<span class="copy">&copy; <?php echo date('Y'); ?> Music Storm. All right reserved.</span> 
			<span class="dev">Made with <a href="" title="">Sumaré</a></span> 
			
		</div>
	</div><!-- footer -->
</footer>
</div><!-- page -->

</body>
</html>
