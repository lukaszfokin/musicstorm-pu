<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
	<div class="content">
		<?php echo $content; ?>
	</div>
</div><!-- content -->
<?php $this->endContent(); ?>