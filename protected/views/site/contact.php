<?php
$this->pageTitle=Yii::app()->name;
Yii::app()->clientScript->registerCss("content","
			.content{
				border-bottom: none;
			}
		");
?>

<div class="contentTitle">	
	<div class="title full">
		<img alt="" src="/musicstorm/images/ms_favicon.png">
		<h2>Contact</h2>
		<h3>If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.</h3>	
	</div>
	
</div>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<?php if(Yii::app()->user->hasFlash('createUser')): ?>

	<div class="alert alert-block alert-success">
		<?php echo Yii::app()->user->getFlash('createUser'); ?>
	</div>

	<?php else: ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'subject',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textAreaRow($model,'body',array('class'=>'span5','maxlength'=>45, 'cols'=>'65')); ?>

	<?php if(CCaptcha::checkRequirements()): ?>
	
		<div style='clear: both;'>	
			<?php $this->widget('CCaptcha'); ?>
		</div>
		<br>
	
		<?php echo $form->textFieldRow($model,'verifyCode',array('class'=>'span2','maxlength'=>100)); ?>
	<?php endif; ?>

	

	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Send',
		)); ?>
	</div>
	
	<?php endif;?>


<?php $this->endWidget(); ?>


<?php endif; ?>