<?php
$this->pageTitle=Yii::app()->name;
Yii::app()->clientScript->registerCss("content","
			.content{
				border-bottom: none;
			}
		");
?>
<div class="contentTitle">	
	<div class="title full">
		<img alt="" src="/musicstorm/images/ms_favicon.png">
		<h2>Login</h2>
		<h3>Login and enjoy the best of music</h3>	
	</div>	
</div>


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->textFieldRow($model,'username',array('class'=>'span5','maxlength'=>100)); ?>
	
	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>100)); ?>
	
	<div class="form-btn">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Send',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

