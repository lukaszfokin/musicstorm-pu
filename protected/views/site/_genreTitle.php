<div class="contentTitle">
	
	<div class="title">
		<img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/ms_favicon.png" />
		<h2><?php echo $title; ?></h2>
		<h3>The best of <?php echo $title; ?>.</h3>	
	</div>
	
	<?php if(!@$hideMore){?>
	<div class="more">
		<a href="<?php echo Yii::app()->createAbsoluteUrl('more', array("genre"=>base64_encode($idGenre)));?>" title="">
			<span class="plus">+</span> More
		</a>
	</div>
	<?php } ?>

</div>